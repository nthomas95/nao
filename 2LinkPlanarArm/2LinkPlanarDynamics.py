import sys
sys.path.insert(0, '..')
import kinematics as k
import dynamics as d
from sympy import symbols, pprint, eye, simplify, Matrix, diff, latex
import sympy
import dill


def main():
    # Some unchanging parameters
    l1, l2 = symbols("l1, l2")                         # arm lengths
    lc1, lc2 = symbols("lc1, lc2")                     # length to COM
    m1, m2 = symbols("m1, m2")                         # masses of the links
    I1, I2, = symbols("I1, I2")                        # moment of inertia of each mass
    theta1, theta2 = symbols("theta1, theta2")         # joint variables
    dtheta1, dtheta2 = symbols("dtheta1, dtheta2")     # joint derivatives
    ddtheta1, ddtheta2 = symbols("ddtheta1, ddtheta2") # joint accelerations
    g = symbols("g")                                   # gravity symbol
    actuators = Matrix([[theta1],
                        [theta2]])
    actuator_derivatives = Matrix([[dtheta1],
                                   [dtheta2]])
    actuator_accelerations = Matrix([[ddtheta1],
                                     [ddtheta2]])
    gravity = Matrix([[0],
                      [g],
                      [0]])

    ##### Mass 1 #####
    data1 = {"xTranslation": [lc1, 0], "zTranslation": [0, 0],
            "xRotation": [0, 0], "zRotation": [theta1, 0],
            "type": ["revolute", "revolute"]}
    chainC1 = k.kinematicChain(data1, actuators)
    #print(chainC1.jointVars)
    #print(chainC1.dhParams)
    #print(chainC1.jacobianMatrix())

    c1_dmatrix = d.dMatrixComponent(m1, chainC1.jacobianMatrix(cutoffFrame=1), chainC1.rotationMatrix(), I1)
    c1_potential_energy = d.potentialEnergy(m1, gravity, chainC1.originVector())

    ##### Mass 2 #####
    data2 = {"xTranslation": [l1, lc2], "zTranslation": [0, 0],
            "xRotation": [0, 0], "zRotation": [theta1, theta2],
            "type": ["revolute", "revolute"]}

    chainC2 = k.kinematicChain(data2, actuators)
    #print(chainC2.dhParams)

    # c2_kineticEnergy = d.kineticEnergy(m2, chainC2.jacobianMatrix(), chainC2.rotationMatrix(), I2, actuator_derivatives)
    c2_dmatrix = d.dMatrixComponent(m2, chainC2.jacobianMatrix(), chainC2.rotationMatrix(), I2)
    c2_potential_energy = d.potentialEnergy(m2, gravity, chainC2.originVector())

    ##### equations of motion #####
    inertia_matrix = c1_dmatrix + c2_dmatrix
    coriolis_matrix = d.cMatrix(inertia_matrix, actuators, actuator_derivatives)  # generate the C matrix from the inertial matrix
    total_potential_energy = c1_potential_energy + c2_potential_energy
    P1 = diff(total_potential_energy, theta1)
    P2 = diff(total_potential_energy, theta2)
    P = Matrix([P1, P2])

    # pprint(inertia_matrix * actuator_accelerations + coriolis_matrix * actuator_derivatives + P)

    # Save the inertia, coriolis, and potential matrices
    dill.dump(inertia_matrix, open("2LinkPlanarInertiaMatrix.dat", 'wb'))
    dill.dump(coriolis_matrix, open("2LinkPlanarCoriolisMatrix.dat", 'wb'))
    dill.dump(total_potential_energy, open("2LinkPlanarPotentialMatrix.dat", 'wb'))


if __name__ == "__main__":
    main()

