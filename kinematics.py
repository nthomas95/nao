import numpy as np
import pandas as pd
from sympy import Matrix, eye, zeros, simplify, cos, sin, symbols, pprint, Number
from sympy.physics.vector import dynamicsymbols


def dhMatrix(xrot, xtrans, zrot, ztrans):
    dhMatrix = np.array([[np.cos(zrot), -np.sin(zrot)*np.cos(xrot), np.sin(zrot)*np.sin(xrot), xtrans*np.cos(zrot)],
                         [np.sin(zrot), np.cos(zrot)*np.cos(xrot), -np.cos(zrot)*np.sin(xrot), xtrans*np.sin(zrot)],
                         [0, np.sin(xrot), np.cos(xrot), ztrans],
                         [0, 0, 0, 1]])
    return dhMatrix


def symdhMatrix(xrot, xtrans, zrot, ztrans):
    dhMatrix = np.array([[cos(zrot), -sin(zrot)*cos(xrot), sin(zrot)*sin(xrot), xtrans*cos(zrot)],
                         [sin(zrot), cos(zrot)*cos(xrot), -cos(zrot)*cos(xrot), xtrans*sin(zrot)],
                         [0, sin(xrot), cos(xrot), ztrans],
                         [0, 0, 0, 1]])
    return dhMatrix


def round_expr(expr, num_digits):
    '''
    loops over each atom in the expression representing a matrix. calls the builtin round function to determine if
    the term should be set to zero.

    :param expr: The matrix (or normal expression) to be rounded
    :param num_digits: How many digits to round to.
    :return: The rounded expression.
    '''
    return expr.xreplace({n: round(n, num_digits) for n in expr.atoms(Number)})


def comCoordinate(masses, coordinates):
    '''
    This function will return the center of mass of a system. Note that it will not return the total mass. That should
    be easy to calculate outside of this function.

    :param masses: The scalar values of the masses
    :param coordinates: The cartesian coordinates of the masses
    :return: The coordinates of the center of mass of the entire system
    '''
    if len(masses) != len(coordinates):
        raise ValueError("Number of masses and coordinates must be equal")

    totalMass = sum(masses)
    com = np.zeros(3)
    for i in range(len(masses)):
        com += masses[i] * coordinates[i]
    com /= totalMass

    return com


class kinematicChain:
    '''
    Returns a kinematicChain object with various methods normally done to calculate
    things like rotation matrices and jacobian matrices.

    '''
    def __init__(self, dhParams, jointVars):
        '''
        Set the provided dhParams as a readable dataframe. Set the joint variables as an array of dynamic symbols

        :param dhParams: A dict-like object that contains ordered pairs of dh parameters
        :param jointVars: An array of order joints variables.
        '''
        self.dhParams = pd.DataFrame(dhParams)
        self.jointVars = jointVars
        #print(self.dhParams)

    def round_expr(self, expr, num_digits):
        '''
        loops over each atom in the expression representing a matrix. calls the builtin round function to determine if
        the term should be set to zero.

        :param expr: The matrix (or normal expression) to be rounded
        :param num_digits: How many digits to round to.
        :return: The rounded expression.
        '''
        return expr.xreplace({n: round(n, num_digits) for n in expr.atoms(Number)})

    def dhMatrix(self, xrot, xtrans, zrot, ztrans):
        '''
        Common normal is orthogonal to both z axis and is the shortest line between them. The new x axis points along
        this common normal and has origin at the intersection with the new z axis.
        https://www.youtube.com/watch?v=rA9tm0gTln8 or search "Denavit-Hartenberg Reference Frame Layout" by Tekkotsu Robotics

        :param xrot: angle about new x from old z to new z
        :param xtrans: length of the common normal
        :param zrot: angle about previous z, from old x to new x
        :param ztrans: offset along previous z to the common normal
        :return: The transformation matrix associated with a single DH frame.
        '''
        A = Matrix(([[cos(zrot), -sin(zrot) * cos(xrot), sin(zrot) * sin(xrot), xtrans * cos(zrot)],
                     [sin(zrot), cos(zrot) * cos(xrot), -cos(zrot) * sin(xrot), xtrans * sin(zrot)],
                     [0, sin(xrot), cos(xrot), ztrans],
                     [0, 0, 0, 1]]))
        return A

    def transformationMatrix(self, frame_numb=False):
        '''
        This will return the transformation matrix from a frame back to the inertial frame. This in contrast to the
        dhMatrix function, this is not between single dh frames but between a chain of frames.

        :param frame_numb: The frame of the desired transformation matrix
        :return: The T transformation matrix (see spong robot modeling and control for notation).
        '''

        T = eye(4)

        if frame_numb:  # is there specified frame number?
            for i in range(frame_numb):
                T = T * self.dhMatrix(self.dhParams.xRotation[i], self.dhParams.xTranslation[i], self.dhParams.zRotation[i],
                                      self.dhParams.zTranslation[i])

        else:  # if no frame number, return the T matrix of the full chain
            for i in range(len(self.dhParams.xTranslation)):
                T = T * self.dhMatrix(self.dhParams.xRotation[i], self.dhParams.xTranslation[i], self.dhParams.zRotation[i],
                                      self.dhParams.zTranslation[i])

        return simplify(T)

    def rotationMatrix(self):
        T = self.transformationMatrix()
        return T[0:3, 0:3]

    def originVector(self):
        T = self.transformationMatrix()
        return T[0:3, 3]

    def jacobianMatrix(self, cutoffFrame=False):
        '''

        :param cutoffFrame: What frame to calculate the jacobian up to. All frames after this will have zero columns.
        :return:
        '''
        def columnCalculator(eeOrigin, frameNumber, type="revolute"):
            '''
            The function will calculate a single column of the Jacobian. It should be called in a loop to fill in the
            columns of a full jacobian matrix.

            :param eeOrigin: Where is the end effector in space? This is usually a symbolic expression although it
                             doesn't need to be.
            :param frameNumber: Which frame in the chain should we find the transformation matrix for.
            :param type: Differentiats between revolute and prismatic joints
            :return: A single column of a jacobian matrix of the form [Jv, Jw]^T
            '''

            if frameNumber == 0:
                currentFrameOrigin = Matrix(3, 1, [0, 0, 0])  # the world origin is always located at (0, 0, 0)
                z = Matrix([[0], [0], [1]])  # z0 is always the unit k vector

            else:
                T = self.transformationMatrix(frameNumber)  # generate the transformation matrix for the current frame
                currentFrameOrigin = T[0:3, 3]  # the currentFrameOrigin is the last first three elements of the last column of T
                z = T[0:3, 0:3]*Matrix([[0], [0], [1]])  # calculate the z vector from the transformation matrix

            # See spong for mathematics behind revolute vs prismatic joints
            if type == "revolute":
                Jv = Matrix(z.cross(eeOrigin - currentFrameOrigin))  # velocity jacobian for revolute joints
                Jw = z

            elif type == "prismatic":
                Jv = z
                Jw = Matrix([[0], [0], [0]])

            else:
                print("Error: type:[\"prismatic\", \"revolute\"]")

            column=Jv.col_join(Jw)  # stack the Jv and Jw vectors on top of one another
            return simplify(column)

        jacobian = zeros(6, len(self.dhParams.xTranslation))  # predefine the jacobian for speed
        T = self.transformationMatrix()  # generate the transformation matrix for the end effector
        # z = T[0:3, 0:3] * Matrix([[0], [0], [1]])  # calculate the z vector from the transformation matrix
        eeOrigin = T[0:3, 3]  # the currentFrameOrigin is the first three elements of the last column of T

        # If a cutoff frame is specified, we only need to go up to that joint
        if cutoffFrame:
            for i in range(cutoffFrame):
                column = columnCalculator(eeOrigin, i, type=self.dhParams.type[i])
                jacobian[:, i] = column

        # If no cutoff frame is specified, go the length of the full chain
        else:
            for i in range(len(self.dhParams.xTranslation)):
                # print(i)
                column = columnCalculator(eeOrigin, i, type=self.dhParams.type[i])
                jacobian[:, i] = column
                # pprint(jacobian)

        jacobian = simplify(jacobian)
        jacobian = self.round_expr(jacobian, 3)  # remove roundoff error terms.
        return jacobian


if __name__ == "__main__":
    masses = [1, 2]
    coordinates = [np.array([1, 1, 1]), np.array([1, 2, 1])]
    print(comCoordinate(masses, coordinates))
