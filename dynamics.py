from sympy import Matrix, eye, zeros, simplify, cos, sin, symbols, pprint, Number, diff


def kineticEnergy(mass, Jacobian, Rot, Inertia, jointVars):
    '''
    This may not be necessary at all

    Use K = (1/2)mv^(T)v + (1/2)w^(T)Iw

    :param mass:
    :param Jacobian:
    :param Rot:
    :param Inertia:
    :param jointVars:
    :return:
    '''
    Jv = Jacobian[0:3, :]  # pull the linear velocity jacobian
    Jw = Jacobian[3:6, :]  # pull the angular velocity jacobian
    translational = mass * Jv.transpose() * Jv  # translational component of the kinetic energy
    rotational = (Jw.transpose() * Rot * Inertia * Rot.transpose() * Jw)  # rotational component of KE
    kEnergy = (1/2) * jointVars.transpose() *(translational + rotational) * jointVars  #
    return simplify(kEnergy)

def dMatrixComponent(mass, Jacobian, Rot, Inertia):
    '''
    This will return the contribution to the inertial matrix

    :param mass:
    :param Jacobian:
    :param Rot:
    :param Inertia:
    :return:
    '''
    Jv = Jacobian[0:3, :]  # pull the linear velocity jacobian
    Jw = Jacobian[3:6, :]  # pull the angular velocity jacobian
    translational = mass * Jv.transpose() * Jv
    rotational = Jw.transpose() * Rot * Inertia * Rot.transpose() * Jw
    return simplify(translational + rotational)

def cMatrix(dMatrix, jointVars, jointDerivs):
    size = len(jointVars)
    C = zeros(size)
    for k in range(size):
        for j in range(size):
            value = 0
            for i in range(size):
                value += (1/2) * (diff(dMatrix[k, j], jointVars[i]) + diff(dMatrix[k, i], jointVars[j])
                                  - diff(dMatrix[i, j], jointVars[k])) * jointDerivs[i]

            C[k, j] = value
    return C

def potentialEnergy(mass, gravity, coordinate):
    P = mass * gravity.transpose() * coordinate

    return P


if __name__ == "__main__":
    pass
    ##### This is for the two link planar manipulator #####
    # l1, l2 = symbols("l1, l2")
    # theta1, theta2 = dynamicsymbols("theta1, theta2")
    # data = {"xTranslation": [l1, 0], "zTranslation": [0, 0],
    #         "xRotation": [0, 0], "zRotation": [theta1, 0],
    #         "type": ["revolute", "revolute"]}
    # actuators = [theta1, theta2]
    # chain = k.kinematicChain(data, actuators)
    #
    # pprint(chain.jacobianMatrix())
