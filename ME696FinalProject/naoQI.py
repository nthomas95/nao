'''
This code will read data from the NAO robot. Use it to subscribe to the top
camera. This will give you an OpenCV compatible image that you can work
with.

python 2.7
'''

# vim: set fileencoding=utf-8 :
import sys
import numpy as np
import cv2
import os
import vision
import dill
from sympy import symbols, lambdify
from naoqi import ALProxy


def getAngles(motionProxy):

    #motionProxy = ALProxy('ALMotion', robotIP, PORT)
    names = ['HeadYaw', 'HeadPitch']
    return motionProxy.getAngles(names, True)


def moveToBox(motionProxy, x, y):

    motionProxy.moveTo(x - 0.15, y, 0)


def main():
    
    # Check if the right sys args were supplied
    if (len(sys.argv) <= 2):
        print "parameter error"
        print "python " + sys.argv[0] + " <ipaddr> <port>"
        sys.exit()

    IP = sys.argv[1]
    PORT = int(sys.argv[2])

    # Create the nao perspective transformation the camera to the waist
    theta1, theta2 = symbols("theta1, theta2")
    jointVariables = [theta1, theta2]
    perspective = dill.load("naoCameraToWaistTransformationMatrix.dat")
    perspective = lambdify(jointVariables, perspective[:3, :], 'numpy')

    # get NAOqi module proxy
    videoDevice = ALProxy('ALVideoDevice', IP, PORT)
    motionProxy = ALProxy('ALMotion', IP, PORT)

    # subscribe top camera
    AL_kTopCamera = 0
    AL_k4VGA = 3  #1280 x 960
    AL_kBGRColorSpace = 13
    captureDevice = videoDevice.subscribeCamera(
        "swear_to_god_this_is_the_last_no_joke", AL_kTopCamera, AL_k4VGA, AL_kBGRColorSpace, 10)

    # create image
    width = 1280
    height = 960
    image = np.zeros((height, width, 3), np.uint8)

    for counter in range(1):

        # get image
        result = videoDevice.getImageRemote(captureDevice)
        print('e')

        if result is None:
            print 'cannot capture.'
        elif result[6] is None:
            print 'no image data string.'
        else:

            # translate value to mat
            values = map(ord, list(result[6]))
            i = 0
            for y in range(0, height):
                for x in range(0, width):
                    image.itemset((y, x, 0), values[i + 0])
                    image.itemset((y, x, 1), values[i + 1])
                    image.itemset((y, x, 2), values[i + 2])
                    i += 3

            #hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
            # show image
            cv2.namedWindow('vision', cv2.WND_PROP_FULLSCREEN)
            cv2.setWindowProperty('vision', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            cv2.imshow('vision', image)
            cv2.waitKey(1)


            # Save Image
            #nameContourImage = 'naoQIImage-1-%d.jpg' % counter
            #cv2.imwrite(nameContourImage, image)
            #cwd = os.getcwd()
            #print(cwd)

            theta1, theta2 = getAngles(motionProxy)
            x, y = vision.triangulateRedBox(image, perspective, theta1, theta2, -0.273)
            moveToBox(motionProxy, x, y)

            while True:

                # exit by [ESC]
                if cv2.waitKey(1) == 27:
                    break

if __name__ == "__main__":
    main()
