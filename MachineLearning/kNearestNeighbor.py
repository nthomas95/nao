import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt


def main():
    # Feature set containing (x,y) values of 25 known/training data
    trainData = np.random.randint(0, 100, (25, 2)).astype(np.float32)

    # Labels each one under either Red or Blue with numbers 0 and 1
    responses = np.random.randint(0, 2, (25, 1)).astype(np.float32)

    # Take the red families and plot them
    red = trainData[responses.ravel() == 0]
    plt.scatter(red[:, 0], red[:, 1], 80, 'r', '^')

    # Take the blue families and plot them
    blue = trainData[responses.ravel() == 1]
    plt.scatter(blue[:, 0], blue[:, 1], 80, 'b', 's')

    newcomer = np.random.randint(0, 100, (1, 2)).astype(np.float32)
    plt.scatter(newcomer[:, 0], newcomer[:, 1], 80, 'g', 'o')

    knn = cv.ml.KNearest_create()
    knn.train(trainData, cv.ml.ROW_SAMPLE, responses)
    ret, results, neighbors, dist = knn.findNearest(newcomer, 3)

    print("result:    {}\n".format(results))
    print("neighbours:    {}\n".format(neighbors))
    print("distance:    {}\n".format(dist))

    plt.show()


if __name__ == "__main__":
    main()

