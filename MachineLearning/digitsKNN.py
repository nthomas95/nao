import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt


def main():
    img = cv.imread("digits.png")
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    # Now we split the image to 5000 cells, each 20x20 in size
    cells = [np.hsplit(row, 100) for row in np.vsplit(gray, 50)]

    # Make it into a numpy array. It will be size (50, 100, 20, 20)
    x = np.array(cells)

    # Prepare some training data and test data
    train = x[:, :90].reshape(-1, 400).astype(np.float32)  # size = (2500,400)
    test = x[:, 90:100].reshape(-1, 400).astype(np.float32)  # size = (2500,400)
    #print(np.shape(train))  # use these to ensure the shapes match up
    #print(np.shape(test))

    # Create labels for the training and test data
    k = np.arange(10)
    train_labels = np.repeat(k, 450)[:, np.newaxis]
    #test_labels = train_labels.copy()
    test_labels = np.repeat(k, 50)[:, np.newaxis]

    # Initiate the kNN, train the data, then test it with test data for k=1
    knn = cv.ml.KNearest_create()
    knn.train(train, cv.ml.ROW_SAMPLE, train_labels)
    ret, result, neighbours, dist = knn.findNearest(test, k=5)

    # Now check the accuracy of the classification
    # For that, compare the result with test_labels and check whic are wrong
    matches = result==test_labels
    correct = np.count_nonzero(matches)
    accuracy = correct * 100.0/result.size
    print(accuracy)

    # Save the data
    #np.savez('knn_data.npz', train=train, train_labels=train_labels)

    # Now load the data
    #with np.load("knn_data.npz") as data:
    #    print(data.files)
    #    train = data['train']
    #    train_labels = data['train_labels']


if __name__ == "__main__":
    main()
