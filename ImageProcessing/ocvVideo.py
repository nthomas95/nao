import numpy as np
import cv2
import scanning

cap = cv2.VideoCapture(0)

while (True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('Webcam', frame)

    # Calculate the mask
    mask = scanning.colorMask(frame, 0, 3, satMin=100)

    # Display the Mask
    cv2.imshow('Mask', mask)

    # Detect quit Key
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()