import numpy as np
import cv2 as cv


def colorMask(img, hue, hueRange, satMin = 40, valMin = 40):
    # Convert from BGR to HSV
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

    # Find lower and upper bounds
    lower = np.uint8((hue-hueRange) % 180)
    upper = np.uint8((hue+hueRange) % 180)

    if lower < upper:
        mask = cv.inRange(hsv, np.array([lower, satMin, valMin]), np.array([upper, 255, 255]))
    else:
        mask = cv.bitwise_or(cv.inRange(hsv, np.array([lower, satMin, valMin]), np.array([179, 255, 255])),
                             cv.inRange(hsv, np.array([0, satMin, valMin]), np.array([upper, 255, 255])))
    return mask


def main():
    # Our images
    img = cv.imread('naoQIImage-1-0.jpg')
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    mask = colorMask(img, 5, 4, satMin=100)

    # filtering the mask
    kernel = np.ones((5, 5), np.uint8)
    filteredMask = cv.morphologyEx(mask, cv.MORPH_OPEN, kernel)
    filteredMask = cv.morphologyEx(filteredMask, cv.MORPH_OPEN, kernel)

    # Displaying the images
    cv.imshow('Original Picture', img)
    cv.waitKey(0)
    # cv.imshow('hsv', hsv)
    # cv.waitKey(0)
    cv.imshow('Mask', mask)
    cv.waitKey(0)
    cv.imshow('Filtered Mask', filteredMask)
    cv.waitKey(0)


if __name__ == "__main__":
    main()
