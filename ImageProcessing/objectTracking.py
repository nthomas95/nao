import cv2 as cv
import numpy as np
from collections import deque
import matplotlib.pyplot as plt
import argparse
import imutils
import time
from imutils.video import VideoStream


# setup our arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", help="Path to the optional video file")
ap.add_argument("-b", "--buffer", type=int, default=64, help="max buffer size")
args = vars(ap.parse_args())

# setup color ranges for green balls
greenLower = (29, 86, 6)
greenUpper = (64, 255, 255)
pts = deque(maxlen=args["buffer"])

# If a video is supplied, grab the video
if args.get("video", True):
    vs = cv.VideoCapture(args["video"])

# If no video was supplied, use the webcam
else:
    vs = VideoStream(src=0).start()

# give the camera / video time to load
time.sleep(2)

while True:
    # Grab the current frame
    frame = vs.read()

    # If a video is not supplied, tell the frame to use Video stream value
    frame = frame[1] if args.get("video", False) else frame

    # If we cannot grab a frame, we have reached the end of a video
    if frame == None:
        break

    

img = cv.imread('opencv-logo.jpeg')

cv.imshow("Original", img)
cv.waitKey(0)


plt.imshow(img)
plt.show()

