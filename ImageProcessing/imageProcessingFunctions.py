import numpy as np
import cv2 as cv


def colorMask(image, hue, hueRange, satMin=40, valMin=40):

    # convert from bgr to hsv
    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    upper = (hue + hueRange) % 180
    lower = (hue - hueRange) % 180

    if lower < upper:
        mask = cv.inRange(hsv, (lower, satMin, valMin), (upper, 255, 255))
    else:
        one = cv.inRange(hsv, (lower, satMin, valMin), (179, 255, 255))
        two = cv.inRange(hsv, (0, satMin, valMin), (upper, 255, 255))
        mask = cv.bitwise_or(one, two)

    return mask


def findContourCenter(cnt):

    M = cv.moments(cnt)
    return int(M['m10']/M['m00']), int(M['m01']/M['m00'])


def verifyContour(cnt, imgSize, areaRatio=0.01, aspectLimit=2, edgeLimit=0.1):
    cntArea = cv.contourArea(cnt)
    imageArea = imgSize[0] * imgSize[1]
    aspectRatio = max(imgSize[0] / imgSize[1], imgSize[1] / imgSize[0])

    # Test if area is to small
    if cntArea < areaRatio*imageArea:
        print("area")
        return False

    # Test if aspect ratio is too large
    if aspectRatio > aspectLimit:
        print("aspect ration")
        return False

    # Test if object is to close to the edge
    d = findContourCenter(cnt)  # find contour center
    if d[1] < imgSize[0]*edgeLimit or d[1] > imgSize[0]*(1-edgeLimit):
        print("location")
        return False
    if d[0] < imgSize[1]*edgeLimit or d[0] > imgSize[1]*(1-edgeLimit):
        print("localtion")
        return False

    return True


def findObjByColor(img, hue, sensitivity, satMin=40, valMin=40):

    # search for the desired object
    mask = colorMask(img, hue, sensitivity, satMin, valMin)

    # Remove filter noise
    kernel = np.ones((7, 7), dtype=np.uint8)
    mask = cv.morphologyEx(mask, cv.MORPH_CLOSE, kernel)
    mask = cv.morphologyEx(mask, cv.MORPH_OPEN, kernel)

    # find contours
    contours, hierarchy = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)

    # Check if contours are found
    if contours == []:
        return None

    # Find the best contour
    cnt = max(contours, key=cv.contourArea)
    cnt = cv.convexHull(cnt)

    # Draw the contour on the original image
    cv.drawContours(img, [cnt], 0, (0, 255, 0), 3)

    # Draw the center point of the contour on the image
    d = findContourCenter(cnt)
    cv.circle(img, (d[0], d[1]), 3, (255, 255, 255), -1)

    # Check if we should accept this shape
    # result = verifyContour(cnt, [img.shape[0], img.shape[1]], edgeLimit=0.1)
    # if result:
    #     print("This is a valid object")
    # else:
    #     print("This is not a valid object")

    return img


def main():
    # Display the original image
    img = cv.imread("oilPlatform.jpeg")
    cv.imshow("Original Image", img)
    cv.waitKey(0)

    # Display the image with found
    mask = findObjByColor(img, 0, 3, satMin=140)
    cv.imshow("Contours", mask)
    cv.waitKey(0)


if __name__ == "__main__":
    main()
