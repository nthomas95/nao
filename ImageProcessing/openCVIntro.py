import cv2 as cv
import numpy as np
import imageProcessingFunctions as ipf
import matplotlib.pyplot as plt



def main():
    cap = cv.VideoCapture(0)
    while True:
        ret, frame = cap.read()
        ipf.findObjByColor(frame, 80, 5, valMin=40, satMin=80)
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        cv.imshow("HSV", hsv)

        cv.imshow("Frame", frame)

        # exit by [ESC]
        if cv.waitKey(33) == 27:
            break

if __name__ == "__main__":
    main()