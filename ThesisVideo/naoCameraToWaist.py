import sys
sys.path.insert(0, '..')
import kinematics as k
import numpy as np
import dill
from sympy import symbols, pprint, lambdify, Matrix, transpose


def generateNaoPerspective():
        
        # Symbolic variables used during transformation calculation
        theta1, theta2 = symbols("theta1, theta2")
        jointVariables = [theta1, theta2]
        
        # Construct the denavit hartenberg parameters in a dictionary
        data = {"xTranslation": [0, 0, 0.05871], "zTranslation": [0.1265, 0, 0.0634],
                "xRotation": [np.pi/2, -np.pi/2, np.pi], "zRotation": [0, theta1, theta2]}
        
        # Create kinematic chain object and generate a transformation matrix
        chain = k.kinematicChain(data, jointVariables)
        tMatrix = chain.transformationMatrix()
        tMatrix = k.round_expr(tMatrix, 5)

        # The given coordinates are correct but need to be rotated. This is easier to do then setting 
        # An additional DH coordinate
        tMatrix = Matrix([[0, 0, 1, 0], [1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1]]).T * tMatrix
        # pprint(tMatrix)

        # return lambdify(jointVariables, tMatrix[:3, :], 'numpy')  # numpy function
        return tMatrix


if __name__ == "__main__":
    perspective = generateNaoPerspective()
    dill.dump(perspective, open("naoCameraToWaistTransformationMatrix.dat", 'wb'))

