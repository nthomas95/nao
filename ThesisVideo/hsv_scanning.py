'''
@author: alexbCreated on Sep 16, 2018


'''
# print( [method_name for method_name in dir(stereo) if callable(getattr(stereo, method_name))])
import numpy as np
import cv2 as cv

# Color detection
BLUE = 0                  # Contour index of blue object
GREEN = 1                 # Contour index of green object
RED = 2                   # Contour index of red object
HSV_BLUE = 120            # HSV value of blue
SENSITIVITY_BLUE = 10     # Color sensitivity of hsv tracking
HSV_GREEN = 50            # HSV value of green
SENSITIVITY_GREEN = 20    # Color sensitivity of hsv tracking
HSV_RED = 5               # HSV value of red
SENSITIVITY_RED = 5       # Color sensitivity of hsv tracking
HSV_SAT_MIN_BLUE = 50     # Minimum saturation for hsv tracking (Used to be 100)
HSV_VAL_MIN_BLUE = 30     # Minimum value for hsv tracking (Used to be 40)
HSV_SAT_MIN_GREEN = 50    # Minimum saturation for hsv tracking (Used to be 10)
HSV_VAL_MIN_GREEN = 145   # Minimum value for hsv tracking (Used to be 145)
HSV_SAT_MIN_RED = 140     # Minimum saturation for hsv tracking (Used to be 40)
HSV_VAL_MIN_RED = 80      # Minimum value for hsv tracking (Used to be 40)

# Filtering
iFILTER_STRENGTH = 150    # Strength of bilateral filter
KERNEL_SIZE = 7           # Size of the morphing kernel of the color mask (noise reduction)
MAX_INTENSITY = 150       # Maximum image intensity (out of 255)

# Watershed Algorithm/Contour Refinement
KERNEL_SIZE_WS = 5        # Size of the morphing kernel for the watershed algorithm
SURE_FG_ITER = 3          # Number of erosion operations for sure foreground
SURE_BG_ITER = 13         # Number of dilation operatins for sure background
EPSILON = 0.01            # Epsilon for contour poly approximation (% of arc length)

# Camera Parameters
FOCAL_LENGTH = 1.2        # Focal length (mm)


# Rotates the point cloud using given rotation matrix
def rotateCloud(cloud, R):
    return R.dot(cloud.T).T


# Outputs Mask indicating where in the image the hue is within the input range
def ColorMask(image, hue, bound, satMin=40, valMin=40):
    # Determine Bounds
    lower = np.uint32((hue - min(bound, 60)) % 180)
    upper = np.uint32((hue + min(bound, 60)) % 180)
              
    if lower < upper:
        mask = cv.inRange(image, np.array([lower, satMin, valMin]), np.array([upper, 255, 255]))
    else:
        mask = cv.bitwise_or(cv.inRange(image, np.array([lower, satMin, valMin]), np.array([179, 255, 255])),
                             cv.inRange(image, np.array([0, satMin, valMin]), np.array([upper, 255, 255])))
    
    return mask


# Outputs mask indicating every point inside the contour
def contourMask(contour, img):
    # pixelpoints = cv.findNonZero(mask)

    mask = np.zeros( (img.shape[0], img.shape[1], 1) , np.uint8)
    cv.drawContours(mask, [contour], 0, 255, -1)
    return mask


# Refines object masks using the watershed algorithm
def watershed(img, masks):
    
    # Initialize kernel, sureBG, and markers
    kernel = cv.getStructuringElement(cv.MORPH_RECT,(KERNEL_SIZE_WS,KERNEL_SIZE_WS))
    sureFG = [None for i in range(len(masks))]
    sureBG = masks[0]
    markers = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
    for i in range(len(masks)):

        # Determine sure foreground of each mask
        sureFG[i] = cv.erode(masks[i], kernel, iterations=SURE_FG_ITER)

        '''
        cv.imshow('Watershed', cv.addWeighted(cv.bitwise_and(img, img, mask=sureFG[i]), 0.8, img, 0.2, 0.0))
        cv.waitKey(0)
        '''

        # Mark sure fg (index starts at 2)
        markers[sureFG[i] == 255] = i + 2

        # Combine masks into sure background
        sureBG = sureBG | masks[i]

    # Build sure background via dilation and inverting
    sureBG = ~cv.dilate(sureBG, kernel, iterations=SURE_BG_ITER)
    markers[sureBG == 255] = 1

    '''
    cv.imshow('Watershed', cv.addWeighted(cv.bitwise_and(img, img, mask=sureBG), 0.8, img, 0.2, 0.0))
    cv.waitKey(0)
    '''
    
    # Apply watershed algorithm
    markers = markers.astype(np.int32)
    cv.watershed(img, markers)
    
    # Aquire new masks from the markers
    for i in range(len(masks)):
        sureFG[i] = (markers == i + 2).astype(np.uint8) * 255

        '''
        cv.imshow('Watershed', cv.addWeighted(cv.bitwise_and(img, img, mask= masks[i]), 0.8, img,0.2, 0.0))
        cv.waitKey(0)
        '''
        
        # Remove noise
        sureFG[i] = cv.morphologyEx(sureFG[i], cv.MORPH_OPEN, kernel, iterations=2)
        '''
        cv.imshow('Watershed', cv.addWeighted(cv.bitwise_and(img, img, mask= sureFG[i]), 0.8, img,0.2, 0.0))
        cv.waitKey(0)
        '''

    cv.destroyAllWindows()
    return sureFG


# Finds the contour of the largest object with given hue
def findObjByColor(bgrImg, hue, sensitivity, satMin=40, valMin=40, verbose=False):
    
    # limit Intensity, Convert to hsv space
    hsv = cv.cvtColor(bgrImg, cv.COLOR_BGR2HSV)
    
    # Search for desired RGB objects
    mask = ColorMask(hsv, hue, sensitivity, satMin, valMin)
    if verbose:
        cv.imshow('vision', mask)
        cv.waitKey(0)
    
    # Remove filter noise
    kernel = cv.getStructuringElement(cv.MORPH_RECT,(KERNEL_SIZE,KERNEL_SIZE))
    mask = cv.morphologyEx(mask, cv.MORPH_CLOSE, kernel)  # Consider adding multiple iterations to each
    mask = cv.morphologyEx(mask, cv.MORPH_OPEN, kernel)
    if verbose:
        cv.imshow('vision', mask)
        cv.waitKey(0)
    
    # Find Contour
    contours, _ = cv.findContours(mask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    
    # Check if contours are found
    if contours == []:
        return None
    
    # Find best contour
    contours[0] = max(contours, key=cv.contourArea)
    contours[0] = cv.convexHull(contours[0])  #TODO: Change with better method
    
    return contours[0]


# Finds the center of the given contour
def findContourCenter(cnt):
    M = cv.moments( cnt )
    return (M['m10']/M['m00'], M['m01']/M['m00'])


# Places a black text label at the given coordinates
def simpleLabel(img, text, center, color=(255, 255, 0), thickness=1, fontSize=0.75):
    labelSize = cv.getTextSize(text, cv.FONT_HERSHEY_TRIPLEX, fontSize, thickness)
    labelCenter = (int(center[0] - (labelSize[0][0]) / 2),
                   int(center[1] + (labelSize[0][1]) / 2))
    cv.putText( img, text, labelCenter, cv.FONT_HERSHEY_TRIPLEX, fontSize, color, thickness=thickness)


# Returns a picture with each contour labeled
def labelContours(img, contours, labels):
    
    labelImg = img.copy()
    
    # Check for matching dimensions
    if len(contours) != len(labels):
        return img
    
    # Label Contours
    for index in range(len(contours)):
        
        # Check if contour exists
        if contours[index] is None:
            continue
    
        # Label contours
        centerX, centerY = findContourCenter(contours[index])
        _,_,_,h = cv.boundingRect(contours[index])
        simpleLabel(labelImg, labels[index], (centerX, centerY - (h)), thickness=2, fontSize=h/71)
        
    return labelImg


# Returns a picture with each contour drawn
def drawContours(img, contours):
        
    labelImg = img.copy()
    
    # Draw Contours
    for index in range(len(contours)):
        
        # Check if contour exists
        if contours[index] is None:
            continue
        
        # Draw contour
        cv.drawContours(labelImg, [contours[index]], 0, (0,0,0), thickness = 2)
        
    return labelImg


# Returns a picture with each contour center  and coordinates drawn
def drawCenters(img, contours, coordinates=None):
        
    labelImg = img.copy()
    
    # Draw Contours
    for index in range(len(contours)):
        
        # Check if contour exists
        if contours[index] is None:
            continue
        
        # Draw Center
        centerX, centerY = np.int16(np.rint(findContourCenter(contours[index])))
        labelImg = cv.circle(labelImg, (centerX, centerY), 2, (255, 255, 255), thickness=2)
        
        # Draw Coordinates
        _,_,_,h = cv.boundingRect(contours[index])
        if coordinates is None:
            simpleLabel(labelImg, "(" + str(centerX) + "," + str(centerY) + ")",
                        (centerX, centerY - round(1.5*h)), thickness=2, fontSize=h/71)
        else:
            simpleLabel(labelImg, "(" + str(int(round(coordinates[index, 0]))) + ","
                        + str(int(round(coordinates[index, 1]))) + ','
                        + str(int(round(coordinates[index, 2]))) + ")",
                        (centerX, centerY - round(1.5*h)), thickness=2, fontSize=h/71)

    return labelImg


# Limits intensity to a specified value   
def limitIntensity(bgrImg, maxIntensity):
    
    # Convert to Lab space, split image
    labImg = cv.cvtColor(bgrImg, cv.COLOR_BGR2Lab)
    
    # Limit range of L channel
    '''
    cv.imshow('intensity', cv.addWeighted(cv.bitwise_and(bgrImg, bgrImg, mask=cv.inRange(labImg,
                    np.array([maxIntensity, 0, 0]), np.array([255, 255, 255]))), 0.8, bgrImg, 0.2, 0.0))
    cv.waitKey(0)
    cv.destroyWindow('intensity')
    '''
    labImg[:, :, 0] = np.minimum(labImg[:, :, 0], maxIntensity)
    img = cv.cvtColor(labImg, cv.COLOR_Lab2BGR)
    '''
    cv.imshow('intensity', img)
    cv.waitKey(0)
    cv.destroyWindow('intensity')
    '''
    
    return img


# Reads in and filters specified image file (in 'Images' directory)
def readPicture(filename):
    img = cv.imread('Images/' + filename)
    #img = cv.bilateralFilter(img, 5, FILTER_STRENGTH, FILTER_STRENGTH)
    return img


# Object that stores the contours of each of the three objects to detect
class BgrObjects:

    # Initialize bgrObjects
    def __init__(self, img, contours=None, verbose=False):
        self.img = img
        
        # Check if contours are passed
        if contours is not None:
            # Extract contour data
            self.blue = contours[BLUE]
            self.green = contours[GREEN]
            self.red = contours[RED]
            
            self.label = self.img.copy()
        else:
            # Find contour data
            self.findContours(verbose=verbose)
        
    # Find all contours
    def findContours(self, verbose=False):
        # Find contours
        self.blue = findObjByColor(self.img, HSV_BLUE, SENSITIVITY_BLUE, satMin=HSV_SAT_MIN_BLUE, valMin=HSV_VAL_MIN_BLUE, verbose=verbose)
        self.green = findObjByColor(self.img, HSV_GREEN, SENSITIVITY_GREEN, satMin=HSV_SAT_MIN_GREEN, valMin=HSV_VAL_MIN_GREEN, verbose=verbose)
        self.red = findObjByColor(self.img, HSV_RED, SENSITIVITY_RED, satMin=HSV_SAT_MIN_RED, valMin=HSV_VAL_MIN_RED, verbose=verbose)
        
        # Label contours
        self.label = self.img.copy()
        
    # Refine contours using watershed algorithm and poly approximation
    def refineContours(self, epsilon= 0):

        # Use watershed algorithm to find blue, red contours
        blueMask, redMask = watershed(self.img, (contourMask(self.blue, self.img), contourMask(self.red, self.img)))
        _,self.blue,_ = cv.findContours(blueMask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        _,self.red,_ = cv.findContours(redMask, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        self.blue = self.blue[0]
        self.red = self.red[0]

        # Use poly approximation on contours
        epB = epsilon * cv.arcLength(self.blue, True)
        epG = epsilon * cv.arcLength(self.green, True)
        epR = epsilon * cv.arcLength(self.red, True)

        self.blue = cv.approxPolyDP(self.blue, epB, True)
        self.green = cv.approxPolyDP(self.green, epG, True)
        self.red = cv.approxPolyDP(self.red, epR, True)
    
    # Label all object contours
    def labelContours(self):
        self.label = labelContours(self.label, self.contours(), ['Obstacle', 'Target', 'Object'])   
        
    # Draw all object contours
    def drawContours(self):
        self.label = drawContours(self.label, self.contours())     
        
    # Draw all contour centers
    def drawCenters(self, coordinates=None):
        self.label = drawCenters(self.label, self.contours(), coordinates)
        
    def drawAll(self, coordinates=None):
        self.drawContours()
        self.labelContours()
        self.drawCenters(coordinates)

    def resetLabel(self):
        self.label = self.img.copy()
    
    # Return all contours as an array
    def contours(self):
        return [self.blue, self.green, self.red]

    def contourMasks(self):
        return contourMask(self.blue, self.img), contourMask(self.green, self.img), contourMask(self.red, self.img)
    
    def gray(self):
        return cv.cvtColor(self.img, cv.COLOR_BGR2GRAY)
