import numpy as np
import hsv_scanning as hsv
import cv2 as cv

# Flag Constants, verifyContour
CNT_DNE = -1           # Contour not found
CNT_INVALID = 0        # Contour not valid
CNT_OUT_OF_BOUNDS = 1  # Contour not within bounds
CNT_VALID = 2          # Contour valid within bounds


# Returns the (u, v) coordinates of the red box, as well as the contour verification check
def findRedBox(img, verbose=False):

    # Find red contour in image
    cnt = hsv.findObjByColor(img, hsv.HSV_RED, hsv.SENSITIVITY_RED, hsv.HSV_SAT_MIN_RED, hsv.HSV_VAL_MIN_RED,
                             verbose=verbose)

    # Draw Contour on image
    if verbose:
        cv.imshow('vision', cv.drawContours(img.copy(), [cnt], -1, (0, 255, 0), thickness=3))
        cv.waitKey(0)

    # Verify contour, find center of red object
    check = verifyContour(cnt, img.shape[:2])
    if check >= CNT_OUT_OF_BOUNDS:
        return cnt, check

    # Return None for a failed check
    return None, check


# Verifies that the red contour represents a valid object
#
# Returns
# -1           (CNT_DNE): Contour not found
#  0       (CNT_INVALID): Contour not valid
#  1 (CNT_OUT_OF_BOUNDS): Contour not within bounds
#  2         (CNT_VALID): Contour valid within bounds
def verifyContour(cnt, imgSize, areaThresh=0.001, aspectThresh=0.25, extentThresh=0.5, boundaryThresh=0.1):

    # Verify the input contour exists
    if cnt is None:
        return CNT_DNE

    # Verify the contour is of a required size
    area = cv.contourArea(cnt)
    print('Contour Area: ', area)
    print('Image Area: ', imgSize[0] * imgSize[1])
    if area < areaThresh * imgSize[0] * imgSize[1]:
        print('too small')
        return CNT_INVALID

    # Verify the contour has a minimum required aspect ratio
    _, (width, height), _ = cv.minAreaRect(cnt)
    if min(width, height) < aspectThresh * max(width, height):
        print('aspect ratio off')
        return CNT_INVALID

    # Verify the contour has a minimum required extent (cnt area vs. bounding rect area)
    if area < extentThresh * width * height:
        print('extent off')
        return CNT_INVALID

    # Verify the contour does not extend to the boundary of the image
    leftMost = tuple(cnt[cnt[:, :, 0].argmin()][0])
    rightMost = tuple(cnt[cnt[:, :, 0].argmax()][0])
    topMost = tuple(cnt[cnt[:, :, 1].argmin()][0])
    bottomMost = tuple(cnt[cnt[:, :, 1].argmax()][0])
    if leftMost[0] < boundaryThresh * imgSize[1] or rightMost[0] > (1 - boundaryThresh) * imgSize[1]:
        return CNT_OUT_OF_BOUNDS
    if topMost[1] < boundaryThresh * imgSize[0] or bottomMost[1] > (1 - boundaryThresh) * imgSize[0]:
        return CNT_OUT_OF_BOUNDS

    # All checks passed
    return CNT_VALID


# Finds the x,y coordinates of the red box given the camera matrix, R, T, image coordinates (u,v), and z coordinate
def determinePosition(cameraMatrix, R, T, u, v, z):

    # Multiply camera matrix, homogeneous transform
    A = cameraMatrix.dot(np.append(R, T[:, np.newaxis], axis=1))

    # Allocate constants A1 - A9
    A1 = A[0, 0]
    A2 = A[0, 1]
    A3 = A[0, 2] * z + A[0, 3]
    A4 = A[1, 0]
    A5 = A[1, 1]
    A6 = A[1, 2] * z + A[1, 3]
    A7 = A[2, 0]
    A8 = A[2, 1]
    A9 = A[2, 2] * z + A[2, 3]

    # Determine x, y
    [x, y] = np.linalg.inv([[A1 - A7 * u, A2 - A8 * u], [A4 - A7 * v, A5 - A8 * v]]).dot([A9 * u - A3, A9 * v - A6])

    return x, y


# Determine the 3D position of the box from an input image
def triangulateRedBox(img, naoPerspective, theta1, theta2, redBoxHeight):

    # Find contour center
    cnt, check = findRedBox(img)
    if check != CNT_VALID:
        return None
    u, v = hsv.findContourCenter(cnt)

    # Find perspectiv transformation
    perspective = naoPerspective(theta1, theta2)
    R = perspective[:, :3]
    T = perspective[:, 3]

    # Determine x, y position
    cameraMatrix = np.array([[1120, 0, 653], [0, 1120, 475], [0, 0, 1]])
    x, y = determinePosition(cameraMatrix, R, T, u, v, redBoxHeight)

    label = hsv.drawContours(img, [cnt])
    label = hsv.labelContours(label, [cnt], ["Red Ball"])
    label = hsv.drawCenters(label, [cnt], np.array([[x * 1000, y * 1000, redBoxHeight * 1000]]))
    print(x, y, redBoxHeight)

    cv.imshow('vision', label)
    cv.waitKey(1)

    return x, y


if __name__ == '__main__':

    # Read in image
    cv.namedWindow('vision', cv.WND_PROP_FULLSCREEN)
    cv.setWindowProperty('vision', cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)
    img = cv.imread('test_images/naoQIImage-3-0.jpg')
    cv.imshow('vision', img)
    cv.waitKey(0)

    # Find contour (u, v) coordinates
    cnt, check = findRedBox(img, verbose=True)

    if check != CNT_VALID:
        print('Contour Not Valid')
        quit()

    label = hsv.drawContours(img, [cnt])
    label = hsv.labelContours(label, [cnt], ['Red Box'])
    label = hsv.drawCenters(label, [cnt])
    cv.imshow('vision', label)
    cv.waitKey(0)

