# https://stackoverflow.com/questions/17185151/how-to-obtain-a-single-channel-value-image-from-hsv-image-in-opencv-2-1

import cv2
from matplotlib import pyplot as plt

# Read image in BGR
img_path = "red.png"
img = cv2.imread(img_path)

# Convert BGR to HSV and parse HSV
hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
h, s, v = hsv_img[:, :, 0], hsv_img[:, :, 1], hsv_img[:, :, 2]

# Plot result images
plt.imshow("Original", img)
plt.imshow("HSV", hsv_img)
plt.imshow("H", h)
plt.imshow("S", s)
plt.imshow("V", v)
plt.show()