import numpy as np
import cv2 as cv


def colorMask(img, hue, hueRange, satMin = 40, valMin = 40):
    #Convert from BGR to HSV
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

    #Find lower and upper bounds
    lower = np.uint8((hue-hueRange) % 180)
    upper = np.uint8((hue+hueRange) % 180)

    if lower < upper:
        mask = cv.inRange(hsv, np.array([lower, satMin, valMin]), np.array([upper, 255, 255]))
    else:
        mask = cv.bitwise_or(cv.inRange(hsv, np.array([lower, satMin, valMin]), np.array([179, 255, 255])),
                             cv.inRange(hsv, np.array([0, satMin, valMin]), np.array([upper, 255, 255])))
    return mask


def finContourCenter(cnt):

    M = cv.moments(cnt)

    return(M['m10']/M['m00'],M['m01']/M['m00'])


def findObjByColor(img, hue, sensitivity, satMin = 40, valMin = 40):

    #Search for desired object
    mask = colorMask(img,hue,sensitivity,satMin,valMin)

    #cv.namedWindow('Before Filtering',cv.WND_PROP_FULLSCREEN)
    #cv.setWindowProperty('Before Filtering',cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)
    #cv.imshow('Before Filtering',mask)
    #cv.waitKey(0)

    #Remove filter noise
    kernel = np.ones((7,7), dtype = np.uint8)
    mask = cv.morphologyEx(mask,cv.MORPH_CLOSE,kernel)
    mask = cv.morphologyEx(mask,cv.MORPH_OPEN,kernel)

    #cv.namedWindow('After Filtering',cv.WND_PROP_FULLSCREEN)
    #cv.setWindowProperty('After Filtering',cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)
    #cv.imshow('After Filtering',mask)
    #cv.waitKey(0)

    #Find contours
    contours,_ = cv.findContours(mask,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)

    #Check if contours are found
    if contours == []:
        return None

    #find best contour
    cnt = max(contours, key= cv.contourArea)
    cnt = cv.convexHull(cnt)

    return cnt


def verifyContour(cnt,imgSize):

    if cnt is None:
        return False

    # cnt: detected countour
    # imgSize: size of the image
    areaContour = cv.contourArea(cnt)
    areaImage = imgSize[0] * imgSize[1];

    areaRatio = areaContour / areaImage
    #print(areaRatio)

    if areaRatio < .05:
        #print('Area Ratio')
        return False

    # Aspect Ratio
    x, y, w, h = cv.boundingRect(cnt)
    aspectRatio = float(w) / h
    #print(aspectRatio)

    if aspectRatio > 2 or aspectRatio < .5:
        #print('Aspect Ratio')
        return False

    # Location of contour
    M = cv.moments(cnt)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])

    if cx > .2 * imgSize[1] and cx < .8 * imgSize[1] and cy > .2 * imgSize[0] and cy < .8 * imgSize[0]:
        return True

    #print('Contour Location')
    return False


if __name__ == "__main__":

    cap = cv.VideoCapture("/dev/4vl/by-path/pci-0000:01:00.0-video-index0")

    for counter in range(5):

        ret, frame = cap.read()
        #img = cv.imread('red_sphere.jpg')
        #imgSize = frame.shape[:2]

        #cv.namedWindow('Original Image',cv.WND_PROP_FULLSCREEN)
        #cv.setWindowProperty('Original Image',cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)
        #cv.imshow('Original Image',frame)
        #cv.waitKey(0)
        nameOriginalImage = 'webcamImage%d.jpg' % counter
        cv.imwrite(nameOriginalImage,frame)

        cnt = findObjByColor(frame, 0, 10, satMin=140)
        cv.drawContours(frame, [cnt], -1, (0, 255, 0), 3)
        #cv.namedWindow('Contour Overlay Picture',cv.WND_PROP_FULLSCREEN)
        #cv.setWindowProperty('Contour Overlay Picture',cv.WND_PROP_FULLSCREEN, cv.WINDOW_FULLSCREEN)
        #cv.imshow('Contour Overlay Picture', frame)
        #cv.waitKey(0)
        nameContourImage = 'contourImage%d.jpg' % counter
        cv.imwrite(nameContourImage,frame)

        # Check if the contour is good
        #print(verifyContour(cnt,imgSize))

    # When everything done, release the capture
    cap.release()
    #cv.destroyAllWindows()
