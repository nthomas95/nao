import numpy as np
import cv2 as cv

if __name__ == "__main__":

img = cv.imread('dice.jpg')

cv.imshow('Red Ball',img)
cv.waitKey(0)

print(img.shape)

'''
img[0:50,50:100] = [0,255,0]
'''

'''
cv.imshow("Image Manipulation",img)
cv.waitKey(0)
'''

'''
cv.imshow('Flip Image',cv.flip(img,1))
cv.waitKey(0)
'''

'''
img[img < 100] = 0
cv.imshow('Black',img)
cv.waitKey(0)
'''

gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
print(gray.shape)
cv.imshow('Gray Image',gray)
cv.waitKey(0)

ret,thresh = cv.threshold(gray,100,255,cv.THRESH_BINARY_INV + cv.THRESH_OTSU)
cv.imshow('Image Threshld',thresh)
cv.waitKey(0)
print(ret)

'''
contours,_ = cv.findContours(thresh,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
cv.drawContours(img, contours, -1, (0, 255, 0), 3)
cv.imshow('Contours', img)
cv.waitKey(0)

cnt = contours[0]
print(cv.contourArea(cnt))
print(cv.arcLength(cnt,True))
print(cv.isContourConvex(cnt))
x, y, w, h = cv.boundingRect(cnt)
print(x, y, w, h)

#Aspect Ratio
print(float(2)/h)

#Contour Center
M = cv.moments(cnt)
cx = int(M['m10']/M['m00'])
cy = int(M['m01']/M['m00'])
print(cx,cy)
'''

kernel = np.ones( (5,5), np.uint8)

erosion = cv.erode(thresh,kernel)
cv.imshow('erode',erosion)
cv.waitKey(0)

dilate = cv.dilate(thresh,kernel)
cv.imshow('dilate',dilate)
cv.waitKey(0)

open = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel)
cv.imshow('open', open)
cv.waitKey(0)

close = cv.morphologyEx(thresh, cv.MORPH_CLOSE, kernel)
cv.imshow('close', close)
cv.waitKey(0)